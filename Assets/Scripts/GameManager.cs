using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum GameState
{
    Running,
    Pause
}

public class GameManager : MonoBehaviour
{


    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {

            if (_instance is null)
            {
                Debug.LogError("GameManager MAnager is null");
            }
            return _instance;
        }
    }
    private static GameState _actualGameState = GameState.Running;
    public static GameState ActualGameState
    {
        get => _actualGameState;
        set => _actualGameState = value;
    }

    private static int _goldCoins = 0;
    public static int GoldCoins
    {
        get => _goldCoins;
        set => _goldCoins = value;
    }

    private static float _timer = 0f;
    public static float Timer
    {
        get => _timer;
        set => _timer = value;
    }
    private void Awake()
    {

        _instance = this;

    }
    public static void Reset()
    {
        _timer = 0;
        _goldCoins=0;
        _actualGameState=GameState.Running;
    }
}
