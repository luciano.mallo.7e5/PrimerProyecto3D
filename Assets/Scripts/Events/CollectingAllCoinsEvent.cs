using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectingAllCoinsEvent : MonoBehaviour
{
    public delegate void CollectingAllCoins();
    public static event CollectingAllCoins CollectingAllCoinsEventComplete;
    private int _totalCoins = 10;//To do catch this value from the spawner.


    private void Update()
    {
        if (GameManager.GoldCoins == _totalCoins)
        {
            CollectingAllCoinsEventComplete();
        }
    }
}
