using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShowFinnishPanel : MonoBehaviour
{
    [SerializeField] private GameObject _panel;
    private void OnEnable()
    {
        _panel = GameObject.Find("FinishPanel");
        CollectingAllCoinsEvent.CollectingAllCoinsEventComplete += ShowPanel;
        _panel.SetActive(false);

    }

    private void ShowPanel()
    {
        GameManager.ActualGameState = GameState.Pause;
        _panel.SetActive(true);
        GameObject.Find("TimeText").GetComponent<Text>().text = "Total Time: " + GameManager.Timer;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) 
        {
            GameManager.Reset();
            LoadScene();
        }
    }

    private void LoadScene()
    {
        SceneManager.LoadScene("ColectingCoins");
    }
    private void OnDisable()
    {
        CollectingAllCoinsEvent.CollectingAllCoinsEventComplete -= ShowPanel;
    }
}
