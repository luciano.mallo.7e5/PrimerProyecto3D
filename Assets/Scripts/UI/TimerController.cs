using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimerController : MonoBehaviour
{

    private float _timer = 0f;

    void Update()
    {
        if (GameManager.ActualGameState == GameState.Running)
        {
            UpdateTimerText();
        }
    }

    private void UpdateTimerText()
    {

        _timer += Time.deltaTime;
        var minutes = _timer / 60;
        var seconds = _timer % 60;
        var miliseconds = (_timer * 100) % 100;
        GameManager.Timer = _timer;
        var textTime = String.Format("{0:00}:{1:00}", minutes, seconds);

        GetComponent<Text>().text = textTime;

    }


}
