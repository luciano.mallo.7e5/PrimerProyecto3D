using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public int gunDamage = 1;
    public float fireRate = 0.25f;
    public float weaponRange = 50f;
    public float hitForce = 100f;
    private float nextFire = 0;
    [SerializeField] private Camera fpsCam;

    public GameObject Bullet;

    private void Awake()
    {
        transform.position = GameObject.Find("Right_IndexProximal").transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        // fpsCam = GetComponentInParent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = GameObject.Find("Right_IndexProximal").transform.position;
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {

            nextFire = Time.time + fireRate;
            Shoot();
        }
    }
    void Shoot()
    {

        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, weaponRange))
        {

            if (hit.collider != null)
            {
                Debug.Log(hit.collider.name);
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.forward * 5f);
    }

}
