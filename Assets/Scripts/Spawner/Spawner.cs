using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    private int _coindToInstantiate = 10;
    public GameObject CoinsToBeInstantiatePrefab;

    private Vector3 _initposition;
    void Start()
    {

    }
    private void Update()
    {
        if (_coindToInstantiate > 0)
        {
            _coindToInstantiate--;
            // CheckWhereCanBeSpawn();
            ChangeInitPosition();
            SpawnObject();
        }

    }


    private void SpawnObject()
    {
        Instantiate(CoinsToBeInstantiatePrefab, _initposition, CoinsToBeInstantiatePrefab.transform.rotation);
    }

    private void CheckWhereCanBeSpawn()
    {
        var r = GameObject.Find("Ground_Mesh").GetComponent<Renderer>();
        var bounds = r.bounds;
        //  do
        {
            float spawnPointX = bounds.center.x + Random.Range(-bounds.extents.x / 2, bounds.extents.x / 2);
            float spawnPointZ = bounds.center.z + Random.Range(-bounds.extents.z / 2, bounds.extents.z / 2);
            _initposition = new Vector3(spawnPointX, 50, spawnPointZ);
        }
        // while (Physics2D.CircleCast(_initposition, 1, Vector3.forward).collider != null);
    }


    private void ChangeInitPosition()
    {
        Collider[] colliders;
        var r = GameObject.Find("Ground_Mesh").GetComponent<Renderer>();
        var bounds = r.bounds;
        do
        {
            _initposition = new Vector3(
                Random.Range(bounds.min.x, bounds.max.x),
                // Random.Range(bounds.min.y, bounds.max.y),
                50f,
                Random.Range(bounds.min.z, bounds.max.z)
            );
            colliders = Physics.OverlapSphere(_initposition, 5f);
        } while (colliders.Length > 0);




    }

}
