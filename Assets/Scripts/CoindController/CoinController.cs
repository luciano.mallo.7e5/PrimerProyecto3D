using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.GoldCoins += 1;
            Debug.Log(GameManager.GoldCoins);
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        var rigidbody = GetComponent<Rigidbody>();
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        GetComponent<SphereCollider>().isTrigger = true;
    }
}
