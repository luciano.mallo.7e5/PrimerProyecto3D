using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartScene : MonoBehaviour
{
    private void OnEnable()
    {

    }

    private void LoadScene()
    {
        SceneManager.LoadScene("ColectingCoins");
    }
}
